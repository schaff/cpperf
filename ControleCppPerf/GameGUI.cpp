#include "GameGUI.h"

GameGUI::GameGUI(IGUIEnvironment *guienv)
{
    //ctor
    this->guienv = guienv;

    //Fen�tre jeu
    fenetreJeu = guienv->addStaticText(L"", rect<int>(0,0,800,400));

    btnStart = guienv->addButton(rect<int>(10, 10, 10+200, 10+100), fenetreJeu, GUI_ID_START, L"DEBUT PARTI");

    afficheMot = guienv->addStaticText(L"", rect<int>(300,10,300+250,10+25), true, true, fenetreJeu);
    afficheMot->setVisible(false);
    btnValid = guienv->addButton(rect<int>(300, 100, 300+100, 100+50), fenetreJeu, GUI_ID_OK, L"OK");
    btnValid->setVisible(false);
    btnReset = guienv->addButton(rect<int>(450, 100, 450+100, 100+50), fenetreJeu, GUI_ID_RESET, L"RESET");
    btnReset->setVisible(false);
    nbChance = guienv->addStaticText(L"NOMBRE DE CHANCE RESTANTE : ",rect<int>(10,5,10+150,5+15), true, true, fenetreJeu);
    nbChance->setVisible(false);
    compteurChance = guienv->addStaticText(L"", rect<int>(10,20, 10+50, 20+25), true, true, fenetreJeu);
    compteurChance->setVisible(false);
    tentative = guienv->addEditBox(L"", rect<int>(375,50,375+100,50+25), true, fenetreJeu);
    tentative->setVisible(false);
    btnAjout = guienv->addButton(rect<int>(475, 50, 475+75, 50+25), fenetreJeu, GUI_ID_AJOUT, L"Ajouter une lettre");
    btnAjout->setVisible(false);
    mot=guienv->addStaticText(L"", rect<int>(300, 200, 300+250, 200+25),true, true, fenetreJeu);
    mot->setVisible(false);

    //gameOver

    lose = guienv->addStaticText(L"TU AS PERDU !!!! GAME OVER",rect<int>(300, 300, 300+200, 300+50), true, true, fenetreJeu);
    lose->setVisible(false);

    //Win

    win = guienv->addStaticText(L"TU AS GAGNE !!!! WIN",rect<int>(300, 300, 300+200, 300+50), true, true, fenetreJeu);
    win->setVisible(false);
}

GameGUI::~GameGUI()
{
    //dtor
}
