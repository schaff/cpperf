#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <irrlicht.h>
#include "enums.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class GameGUI
{
    public:
        GameGUI(IGUIEnvironment *guienv);
        virtual ~GameGUI();

        IGUIStaticText *fenetreJeu;

        IGUIButton *btnReset;
        IGUIButton *btnValid;
        IGUIButton *btnAjout;
        IGUIStaticText *compteurChance;
        irr::gui::IGUIEditBox *tentative;
        IGUIStaticText *afficheMot;
        IGUIButton *btnStart;
        IGUIStaticText *nbChance;
        IGUIStaticText *mot;
        IGUIStaticText *lose;
        IGUIStaticText *win;

    protected:

    private:
        IGUIEnvironment *guienv;


};

#endif // GAMEGUI_H
