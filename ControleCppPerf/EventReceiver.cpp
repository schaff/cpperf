#include "EventReceiver.h"
#include "game.h"
#include <iostream>

EventReceiver::EventReceiver(Game *pendu)
{
    //ctor
    this->pendu = pendu;
}
EventReceiver::~EventReceiver()
{
    //dtor
}

bool EventReceiver::OnEvent(const SEvent& event)
{
    if (event.EventType == EET_GUI_EVENT)
    {
        s32 id = event.GUIEvent.Caller->getID();

        switch(event.GUIEvent.EventType)
        {
        case EGET_BUTTON_CLICKED:
            std::cout << "Clic sur " << id << std::endl;
            switch(id)
            {
            case GUI_ID_START:
                {
                    ///AFFICHE LES ELEMENTS UTILES POUR JOUER
                    irr::core::stringw txt = "7";
                    pendu->gui->compteurChance->setText(txt.c_str()); // mise a z�ro du compteur pour le d�but de la partie
                    pendu->gui->afficheMot->setVisible(true);
                    pendu->gui->tentative->setVisible(true);
                    pendu->gui->btnAjout->setVisible(true);
                    pendu->gui->btnReset->setVisible(true);
                    pendu->gui->btnValid->setVisible(true);
                    pendu->gui->btnStart->setVisible(false);
                    pendu->gui->compteurChance->setVisible(true);
                    pendu->gui->nbChance->setVisible(true);
                    pendu->gui->mot->setVisible(true);

                    /// RECHERCHE UN MOT AU HASARD POUR JOUER
                    srand(time(NULL)); //init de la fonction al�atoire
                    std::fstream fs;
                    fs.open("liste.txt", std::fstream::in);//ouverture du fichier txt
                    fs.seekg(0, std::fstream::end);//calcul de la taille du fichier
                    int taille = fs.tellg(); //on met en memoir sa taille
                    if (fs.is_open())
                    {
                        int ligneAleatoire(0);
                        ligneAleatoire=rand()%taille;
                        getline(fs, motAleatoire); //selection d'un mot al�atoirement
                        vMot.reserve(motAleatoire.size()); //regarde la taille du mot
                        for (size_t lettre =0; lettre<motAleatoire.size(); ++lettre) //mise de chaque charact�re dans le vecteur
                        {
                            //vMot.push_back(motAleatoire[lettre].c_str());
                        }
                    }
                    fs.close();


                    return true;
                }

            case GUI_ID_RESET:
                {
                    pendu->gui->btnValid->setVisible(true);
                    pendu->gui->btnAjout->setVisible(true);
                    pendu->gui->lose->setVisible(false);
                    pendu->gui->win->setVisible(false);
                    irr::core::stringw txt = "7";
                    pendu->gui->compteurChance->setText(txt.c_str()); // mise a z�ro du compteur pour le reset de la partie
                    irr::core::stringw mot="";
                    pendu->gui->afficheMot->setText(mot.c_str());// reinitialise l'�ditBox pour un confort

                    pendu->vLettre.clear(); // nettoie les vecteurs pour une nouvelle partie
                    vMot.clear(); // nettoie les vecteurs pour une nouvelle partie


                    int random = rand()%10+1; // on choisit un chiffre pour selectionner un mot al�atoirement dans le fichier texte

                    return true;
                }

            case GUI_ID_AJOUT:
                {
                    irr::core::stringw lettre = pendu->gui->tentative->getText();
                    pendu->vLettre.push_back(lettre);//Ajout des lettres du mot dans un vecteur
                    irr::core::stringw mot = pendu->gui->afficheMot->getText();
                    mot += pendu->gui->tentative->getText();
                    pendu->gui->afficheMot->setText(mot.c_str()); // ajout des lettre dans la staticBox
                    irr::core::stringw txt = ""; // remise a z�ro de l'editBox
                    pendu->gui->tentative->setText(txt.c_str());// reinitialise l'�ditBox pour un confort
                    return true;
                }

            case GUI_ID_OK:
                {
                    irr::core::stringw afficheMo = "";
                    pendu->gui->tentative->setText(afficheMo.c_str());// reinitialise la staticBox pour un confort




                    ///TEST DES COMBINAISAONS DE LETTRE
                    for (int i=0; i<vMot.size(); ++i)
                    {
                        if (pendu->vLettre[i] == vMot[i])
                        {
                            pendu->test=true;
                        }
                        else if (pendu->vLettre[i] != vMot[i])
                        {
                            pendu->test=false;
                        }
                        if (pendu->test == true) // si on a trouv� une bonne lettre elle s'affiche dans une static
                        {
                            irr::core::stringw txt = pendu->gui->mot->getText();
                            txt += pendu->vLettre[i];
                            pendu->gui->mot->setText(txt.c_str());
                        }
                        else if (pendu->test==false) // si on c'est tromp� il y a _ qui s'affiche
                        {
                            irr::core::stringw txt = pendu->gui->mot->getText();
                            txt += "_";
                            pendu->gui->mot->setText(txt.c_str());
                        }
                    }

                    /// VERIF SI LE JOUEUR A GAGNE


                    /// CONTROLE EFFET SI LE JOUEUR A GAGNE OU PERDU
                    irr::core::stringw memoir = pendu->gui->compteurChance->getText();
                    if (memoir== "0") // v�rifie si le compteur n'est pas a 0 et s'il est a 0 le joueur perd
                    {
                        pendu->gui->lose->setVisible(true);
                        pendu->gui->btnValid->setVisible(false);
                        pendu->gui->btnAjout->setVisible(false);
                    }
                    if (pendu->gagne == true) //si le joueur a gagn� on le renvoie vers une nouvelle fenetre
                    {
                        pendu->gui->btnValid->setVisible(false);
                        pendu->gui->btnAjout->setVisible(false);
                        pendu->gui->win->setVisible(true);
                    }
                    else if(pendu->gagne == false) //si le joueur n'a pas perdu / ni gagn� le nombre de chance pour trouver descend d'un chiffre
                    {
                        if (memoir=="7")
                        {
                            irr::core::stringw txt = "6";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="6")
                        {
                            irr::core::stringw txt = "5";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="5")
                        {
                            irr::core::stringw txt = "4";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="4")
                        {
                            irr::core::stringw txt = "3";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="3")
                        {
                            irr::core::stringw txt = "2";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="2")
                        {
                            irr::core::stringw txt = "1";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }
                        else if (memoir=="1")
                        {
                            irr::core::stringw txt = "0";
                            pendu->gui->compteurChance->setText(txt.c_str());
                        }

                    }
                    return true;
                }

            default:
                return false;
            }
            break;
        default:
            break;
        }
    }
    return false;
}

