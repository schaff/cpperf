#ifndef GAME_H
#define GAME_H

#include <vector>

#include <iostream>
#include <irrlicht.h>
#include <string>
#include "GameGUI.h"
#include "EventReceiver.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game
{
    public:
        Game();
        virtual ~Game();

        void run();

        GameGUI *gui;
        bool test;
        bool gagne;
        std::vector <irr::core::stringw> vLettre;



    protected:

    private:
        IrrlichtDevice *device;
        IVideoDriver *driver;
        IGUIEnvironment *guienv;

        EventReceiver *receiver;
};

#endif // GAME_H
