#include "Game.h"

int main()
{
    //Classe principale du jeu
	Game *pendu = new Game();

	//Boucle principale
    pendu->run();

    //Destructeur
    delete pendu;

    return 0;
}
