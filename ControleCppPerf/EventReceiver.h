#ifndef EVENTRECEIVER_H
#define EVENTRECEIVER_H

#include <irrlicht.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <time.h>
#include <stdlib.h>
#include "enums.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

class Game;

class EventReceiver : public IEventReceiver
{
    public:
        EventReceiver(Game *pendu);
        virtual ~EventReceiver();

        virtual bool OnEvent(const SEvent& event);
    protected:

    private:
        Game *pendu;
        std::vector <irr::core::stringw> vMot;
        std::string motAleatoire;

};

#endif // EVENTRECEIVER_H
