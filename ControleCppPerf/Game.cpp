#include "Game.h"

Game::Game()
{
    //ctor
    //Cr�er le device irrlicht
    device = createDevice(EDT_SOFTWARE, dimension2d<u32>(800, 400), 16,
            false, false, false, 0);

    driver = device->getVideoDriver();
    guienv = device->getGUIEnvironment();

    //Cr�ation de la classe GUI
    gui = new GameGUI(guienv);

    //Event receiver
    receiver = new EventReceiver(this);

    //Lien entre le Device et l'event receiver
    device->setEventReceiver(receiver);

    test=false;
    gagne = false;

}

Game::~Game()
{
    //dtor
    device->drop();
}

void Game::run()
{
 while(device->run())
 {
        driver->beginScene(true, true, SColor(0,200,200,200));

        guienv->drawAll();

        driver->endScene();
 }
}
